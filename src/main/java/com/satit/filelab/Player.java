package com.satit.filelab;

import java.io.Serializable;

/**
 *
 * @author satit
 */
public class Player implements Serializable {

    private char symbol;
    private int win;
    private int lose;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public int getLose() {
        return lose;
    }

    public void lose() {
        this.lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }

}
